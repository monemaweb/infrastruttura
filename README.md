# Configurazioni Ansible

## Eseguire un playbook su un host specifico

ansible-playbook   playbooks/groups/webserver.yaml --limit pratosfera.monema.cloud
no_proxy='*' ANSIBLE_KEEP_REMOTE_FILES=1 ANSIBLE_DEBUG=1 ansible-playbook playbooks/groups/webserver.yaml --limit pratosfera.monema.cloud

## Odoo

Installazione di un Odoo con Nginx/SSL (Certbot) 

 ansible-playbook  -i hosts.yaml playbooks/groups/odoo.yaml -u root --limit moreeurope.monema.cloud
